<?php
/**
 * Author: Maksym Kawelski
 */

namespace Lottery;

class ElGordo extends Lottery
{

    public function __construct()
    {
        // set config data
        $this->setLotteryName("ElGordo");
        $this->setLotteryUrl("https://www.elgordo.com/results/euromillonariaen.asp");
    }

    public function getLotteryResults ()
    {

        $arrayTemp = Array();
        $draws = null;

        // searching results
        preg_match_all('#<span class="int-num"[^>]*>(.*?)</span>#', $this->getContentsSite(),$draws);

        // add results to temp array
        for ($x = 0; $x < 7; $x++)
        {
            $arrayTemp[] = (int)strip_tags($draws[0][$x]);
        }

        // set output array
        $arrayOutput = Array();
        $arrayOutput[$this->getNameLottery()] = $arrayTemp ;

        $this->setArrayOutput($arrayOutput);

    }

}