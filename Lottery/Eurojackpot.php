<?php
/**
 * Author: Maksym Kawelski
 */

namespace Lottery;

class Eurojackpot extends Lottery
{

    public function __construct()
    {
        // set config data
        $this->setLotteryName("Eurojackpot");
        $this->setLotteryUrl("https://www.lotto.pl/eurojackpot/wyniki-i-wygrane");
    }

    public function getLotteryResults ()
    {

        $arrayTemp = Array();
        $draws = null;

        // searching results
        preg_match_all('#<div class="number text-center"[^>]*>(.*?)</span>#', $this->getContentsSite(),$draws);

        // add results to temp array
        for ($x = 0; $x <= 7; $x++)
        {
            $arrayTemp[] = (int) strip_tags($draws[0][$x]);
        }

        // set output array
        $arrayOutput = Array();
        $arrayOutput[$this->getNameLottery()] = $arrayTemp ;

        $this->setArrayOutput($arrayOutput);

    }

}

?>