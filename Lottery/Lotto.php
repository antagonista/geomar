<?php
/**
 * Author: Maksym Kawelski
 */

namespace Lottery;

class Lotto extends Lottery
{

    public function __construct()
    {
        // set config data
        $this->setLotteryName("Lotto/LottoPlus/SuperSzansa");
        $this->setLotteryUrl("https://www.lotto.pl/lotto/wyniki-i-wygrane");
    }

    public function getLotteryResults ()
    {

        $lottoTemp = Array();
        $lottoPlusTemp = Array();
        $superSzansaTemp = Array();

        $draws = null;

        // searching results
        preg_match_all('#<div class="number text-center"[^>]*>(.*?)</span>#', $this->getContentsSite(),$draws);

        // lotto lottery: add results to temp array
        for ($x = 0; $x <= 5; $x++)
        {
            $lottoTemp[] = (int) strip_tags($draws[0][$x]);
        }

        // lotto plus lottery: add results to temp array
        for ($x = 18; $x <= 23; $x++)
        {
            $lottoPlusTemp[] = (int) strip_tags($draws[0][$x]);
        }

        // super szansa lottery: add results to temp array
        for ($x = 36; $x <= 42; $x++)
        {
            $superSzansaTemp[] = (int) strip_tags($draws[0][$x]);
        }

        // set output array
        $arrayOutput = Array();
        $arrayOutput["Lotto"] = $lottoTemp;
        $arrayOutput["LottoPlus"] = $lottoPlusTemp;
        $arrayOutput["SuperSzansa"] = $superSzansaTemp;

        $this->setArrayOutput($arrayOutput);

    }

}


?>