<?php
/**
 * Author: Maksym Kawelski
 */

namespace Lottery;

class Lottery
{

    private $lotteryName = null;
    private $lotteryUrl = null;
    private $contents = null;
    private $arrayOutput = null;

    public function setLotteryName ($name)
    {
        $this->lotteryName = $name;
    }

    public function setLotteryUrl ($url)
    {
        $this->lotteryUrl = $url;
    }

    public function setArrayOutput($array)
    {
        $this->arrayOutput = $array;
    }

    public function getNameLottery()
    {
        return $this->lotteryName;
    }

    public function getContentsSite ()
    {
        return $this->contents;
    }

    public function getLotteryResults ()
    {
        return true;
    }

    private function getSite ()
    {

        // handle site url
        $lines_array=file($this->lotteryUrl);
        // get contents site array to string
        $lines_string=implode('',$lines_array);

        // set class variable
        $this->contents = $lines_string;

    }

    public function getOutput ()
    {

        if ($this->lotteryName != null and $this->lotteryUrl != null)
        {
            // get contents site
            $this->getSite();

            // get won results
            $this->getLotteryResults();

            // check results array
            if (is_array($this->arrayOutput) and !empty($this->arrayOutput))
            {
                return $this->arrayOutput;
            }
            else return Array();

        } else return Array();

    }

}

?>