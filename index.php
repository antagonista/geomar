<?php
/**
 * Author: Maksym Kawelski
 */

// include classes
include_once "Lottery/Lottery.php";
include_once "Lottery/ElGordo.php";
include_once "Lottery/Eurojackpot.php";
include_once "Lottery/Lotto.php";

// class initiation
$elGordoLottery = new \Lottery\ElGordo();
$eurojackpotLottery = new \Lottery\Eurojackpot();
$lottoLotteries = new \Lottery\Lotto();

// set output array
$tempArray = Array();
$tempArray = array_merge($elGordoLottery->getOutput(), $eurojackpotLottery->getOutput(), $lottoLotteries->getOutput());

// save JSON output to file
file_put_contents("output.json", json_encode($tempArray));

?>